function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  test('when name size in lower than expected', () => {
    expect(() => createUser('', 'oi@oi.com.br', 'xxxxxx', 'xxxxxxx')).toThrowError('validation error: invalid name size');
  })

  test('when name size in bigger than expected', () => {
    expect(() => createUser('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'oi@oi.com', 'xxxxxx', 'xxxxxxx')).toThrowError('validation error: invalid name size');
  })

  test('when email is not a valid email', () => {
    expect(() => createUser('Isabella', 'oi@oi', 'xxxxxx', 'xxxxxxx')).toThrowError('validation error: invalid email format');
  })

  test('when password length is lower than expected', () => {
    expect(() => createUser('Isabella', 'oi@oi.com.br', 'x', 'x')).toThrowError('validation error: invalid password format');
  })

  test('when passwords do not match', () => {
    expect(() => createUser('Isabella', 'oi@oi.com.br', 'aaaaaaa', 'xxxxxxx')).toThrowError('validation error: confirmation does not match');
  })

  test('when all information is valid', () => {
    expect(() => createUser('Isabella', 'oi@oi.com.br', 'aaaaaaa', 'aaaaaaa')).not.toThrow();
  })

});
